import axios from "axios";

const GROCERY_REPORT_REST_API_URL = 'http://localhost:9003/groceryReport/getReport?itemName=';

class ReportService {
    generateReport(name) {
        return axios.get(GROCERY_REPORT_REST_API_URL + name, {responseType: 'blob', timeout: 30000});
    }
}
export default new ReportService();
import axios from "axios";

const GROCERY_REST_API_URL = 'http://localhost:9002/grocery/getAllGrocery?page=';

class GroceryService {
    getAllGrocery(page, size) {
        return axios.get(GROCERY_REST_API_URL + page + '&size=' + size);
    }
}
export default new GroceryService();
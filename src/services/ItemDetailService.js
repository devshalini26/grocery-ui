import axios from "axios";

const GROCERY_NAME_REST_API_URL = 'http://localhost:9002/grocery/getGroceryNames';
const GROCERY_NAME_DETAILS_REST_API_URL = 'http://localhost:9002/grocery/getGroceryByName?itemName=';

class ItemDetailService {
    getGroceryNames() {
        return axios.get(GROCERY_NAME_REST_API_URL);
    }

    getGroceryByName(name) {
        return axios.get(GROCERY_NAME_DETAILS_REST_API_URL + name);
    }
}

export default new ItemDetailService();
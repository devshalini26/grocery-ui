import './App.css';

import {Container, Row, Col} from 'react-bootstrap';
import {BrowserRouter, Routes, Route} from 'react-router-dom';

import NavigationBar from "./components/NavigationBar";
import WelcomeComponent from "./components/WelcomeComponent";
import GroceryComponent from "./components/GroceryComponent";
import ItemDetailComponent from "./components/ItemDetailComponent";
import ReportComponent from "./components/ReportComponent";

function App() {
  const marginTop = {
    marginTop: "20px"
  };

  return (
        <BrowserRouter>
            <NavigationBar />
            <Container>
                <Row>
                    <Col lg={20} style={marginTop}>
                        <Routes>
                            <Route path="/" element={<WelcomeComponent />} />
                            <Route path="/maxPriceItem" element={<GroceryComponent />} />
                            <Route path="/itemDetailsByName" element={<ItemDetailComponent />} />
                            <Route path="/itemReport" element={<ReportComponent />} />
                            <Route path="*" element={<main style={{ padding: "1rem" }}><p>There's nothing here!</p></main>} />
                        </Routes>
                    </Col>
                </Row>
            </Container>
        </BrowserRouter>
      );
}

export default App;

import React, { Component } from 'react';
import Select from 'react-select';
import {InputGroup, Button} from 'react-bootstrap'
import {Line} from 'react-chartjs-2';
import {Chart, CategoryScale, LinearScale, PointElement, LineElement} from 'chart.js';
import FileSaver from 'file-saver';

import "react-datepicker/dist/react-datepicker.css";

import ItemDetailService from '../services/ItemDetailService';
import ReportService from '../services/ReportService';

Chart.register(CategoryScale, LinearScale, PointElement, LineElement);

class ReportComponent extends Component {
  constructor() {
      super();
      this.state = {
        itemDetails: [],
        itemDate: [],
        itemPrice: [],
        selectedOption: '',
        startDate: new Date()
      }
  }

  async getOptions(){
      const res = await ItemDetailService.getGroceryNames();
      const data = res.data;

      const options = data.map(d => ({
        "value" : d,
        "label" : d

      }));

      this.setState({selectOptions: options});

    }

  componentDidMount(){
    this.getOptions();
  }

  handleOnChange(e) {
      if(e) {
        ItemDetailService.getGroceryByName(e.value).then((response) => {
                this.setState({
                    itemDetails: response.data,
                    itemDate: [],
                    itemPrice: [],
                    selectedOption: e.value
                });
                this.state.itemDetails.map((item) => {
                    this.state.itemDate.push(item.priceDate);
                    this.state.itemPrice.push(item.price);
                });
          });
      }
  }

  generateReport() {
    ReportService.generateReport(this.state.selectedOption).then((response) => {
        FileSaver.saveAs(response.data, 'report.jpg');
    });
  }

  render() {
    const {itemDate, itemPrice, itemDetails} = this.state;
    return (
        <div>
            <InputGroup size="sm">
                <Select closeMenuOnSelect={true} placeholder="Select Item Name" noOptionsMessage={()=> 'Item Not Found'}
                                options={this.state.selectOptions} onChange={this.handleOnChange.bind(this)} isSearchable isClearable  />

                <Button onClick={this.generateReport.bind(this)}>Generate Report</Button>
            </InputGroup>
            <hr />
            {itemDetails.length === 0 ? '' :
            <div>
                <Line data=
                    {{
                          labels: itemDate,
                          datasets: [
                            {
                              label: 'Price',
                              data: itemPrice,
                              backgroundColor: [
                                'rgba(255, 99, 132, 0.2)'
                              ],
                              borderColor: [
                                'rgba(255, 99, 132, 1)'
                              ],
                              borderWidth: 1,
                            },
                          ],
                        }}
                        height={400}
                        width={600}
                        options={{
                              maintainAspectRatio: false,
                              scales: {
                              y: {
                                    suggestedMin: 0,
                                    suggestedMax: 500
                                 },
                              },
                              legend: {
                                labels: {
                                  fontSize: 25,
                                },
                              },
                            }}
                      />
            </div>
            }
        </div>
    );
  }
}

export default ReportComponent;
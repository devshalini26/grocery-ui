import React, {Component} from "react";
import {Card, Table, InputGroup, Button} from 'react-bootstrap'
import GroceryService from "../services/GroceryService";

class GroceryComponent extends Component {
    constructor() {
        super();
        this.state = {
            groceries: [],
            currentPage: 1,
            groceriesPerPage: 20
        }
    }
    componentDidMount() {
        this.findAllGroceries(this.state.currentPage);
    }

    findAllGroceries(currentPage) {
        currentPage -= 1;
        GroceryService.getAllGrocery(currentPage, this.state.groceriesPerPage).then((response) => {
            this.setState({
                groceries: response.data.content,
                totalPages: response.data.totalPages,
                totalElements: response.data.totalElements,
                currentPage: response.data.number + 1
            });
        });
    }

    firstPage = () => {
        let firstPage = 1;
        if(this.state.currentPage > firstPage) {
            this.findAllGroceries(firstPage);
        }
    };

    prevPage = () => {
        let prevPage = 1;
        if(this.state.currentPage > prevPage) {
            this.findAllGroceries(this.state.currentPage - prevPage);
        }
    };

    nextPage = () => {
        if(this.state.currentPage < Math.ceil(this.state.totalElements / this.state.groceriesPerPage)) {
        this.findAllGroceries(this.state.currentPage + 1);
        }
    };

    lastPage = () => {
        let condition = Math.ceil(this.state.totalElements / this.state.groceriesPerPage);
        if(this.state.currentPage < condition) {
            this.findAllGroceries(condition);
        }
    };

    render() {
        const {groceries, currentPage, totalPages} = this.state;

        return (
            <div>
                <Card className={"border border-dark"}>
                    <Card.Header>Grocery List</Card.Header>
                    <Card.Body>
                        <Table bordered hover striped>
                            <thead>
                                <tr>
                                    <td> Item Name </td>
                                    <td> Item Max Price </td>
                                    <td> Date </td>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    groceries.length === 0 ? <tr align="center"><td colSpan="3">No Groceries Available</td></tr> :
                                    groceries.map(grocery =>
                                        <tr>
                                            <td>{grocery.itemName}</td>
                                            <td>{grocery.price}</td>
                                            <td>{grocery.priceDate}</td>
                                        </tr>
                                    )
                                }
                            </tbody>
                        </Table>
                    </Card.Body>
                    <Card.Footer>
                        <div style={{"float":"left"}}>
                            Showing Page {currentPage} of {totalPages}
                        </div>
                        <div style={{"float":"right"}}>
                            <InputGroup size="sm">
                                <Button variant="outline-info" disabled={currentPage === 1 ? true : false} onClick={this.firstPage}>First</Button>
                                <Button variant="outline-info" disabled={currentPage === 1 ? true : false} onClick={this.prevPage}>Prev</Button>

                                <Button variant="outline-info" disabled={currentPage === totalPages ? true : false} onClick={this.nextPage}>Next</Button>
                                <Button variant="outline-info" disabled={currentPage === totalPages ? true : false} onClick={this.lastPage}>Last</Button>
                            </InputGroup>
                        </div>
                    </Card.Footer>
                </Card>
            </div>
        );
    }
}

export default GroceryComponent
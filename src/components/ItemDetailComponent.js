import React, { Component } from 'react';

import Select from 'react-select';
import {Card, Table} from 'react-bootstrap'

import ItemDetailService from '../services/ItemDetailService';

class ItemDetailComponent extends Component {
    constructor() {
        super();
        this.selectRef = React.createRef();
        this.state = {
          selectOptions : [],
          value : '',
          itemDetails: [],
          filteredItemDetails: [],
          priceOptions: []
        }
      }

      async getOptions(){
        const res = await ItemDetailService.getGroceryNames();
        const data = res.data;

        const options = data.map(d => ({
          "value" : d,
          "label" : d
        }));
        this.setState({selectOptions: options});
      }

      componentDidMount(){
        this.getOptions();
      }

      handleOnChange(e) {
        if (e) {
          ItemDetailService.getGroceryByName(e.value).then((response) => {
              this.setState({
                  itemDetails: response.data,
                  filteredItemDetails: response.data
              });
              const uniquePrice = [...new Map(this.state.itemDetails.map(item => [item['price'], {
                "value" : item.price,
                "label" : item.price
              }])).values()];
              uniquePrice.sort((a,b) => (a.value > b.value) ? 1 : ((b.value > a.value) ? -1 : 0));

              this.setState({priceOptions: uniquePrice});
              this.clearValue();
          });
        }
      }

      handleOnChangeForFilter(e) {
        if (e) {
          const tempItemDetails = [];
          this.state.itemDetails.filter((val) => {
              if(val.price === e.value) { tempItemDetails.push(val);}
          });
          this.setState({filteredItemDetails: tempItemDetails});
        }
      }

      clearValue() {
        if(this.selectRef.current) {
          this.selectRef.current.clearValue();
        }
      };

      render() {
        const {filteredItemDetails} = this.state;
        return (
          <div>
            <Select closeMenuOnSelect={true} placeholder="Select Item Name" noOptionsMessage={()=> 'Item Not Found'}
                options={this.state.selectOptions} onChange={this.handleOnChange.bind(this)} isSearchable isClearable />
            <hr />

            <div>
                 <Card className={"border border-dark"}>
                     <Card.Header>Item Details</Card.Header>
                     <Card.Body>
                         <Table bordered hover striped>
                             <thead>
                                 <tr>
                                     <td> Item Name </td>
                                     <td> Item Max Price 
                                     <Select ref={this.selectRef} classNamePrefix="select" 
                                     closeMenuOnSelect={true} placeholder="Select Price" options={this.state.priceOptions} 
                                     onChange={this.handleOnChangeForFilter.bind(this)} />
                                     </td>
                                     <td> Date </td>
                                 </tr>
                             </thead>
                             <tbody>
                                 {
                                     filteredItemDetails.length === 0 ? <tr align="center"><td colSpan="3">No Item Selected</td></tr> :
                                     filteredItemDetails.map(item =>
                                         <tr>
                                             <td>{item.itemName}</td>
                                             <td>{item.price}</td>
                                             <td>{item.priceDate}</td>
                                         </tr>
                                     )
                                 }
                             </tbody>
                             <tfoot>
                               {
                                 filteredItemDetails.length === 0 ? '' : <tr align="center"><td colSpan="3">Total Items: {filteredItemDetails.length}</td></tr>
                               }
                             </tfoot>
                         </Table>
                     </Card.Body>
                 </Card>
             </div>
          </div>
        );
      }
}

export default ItemDetailComponent;
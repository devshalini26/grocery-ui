import React, {Component} from 'react';

class WelcomeComponent extends Component {
    render() {
     return (
        <div class="bg-dark text-white p-4 rounded-lg m-3">
            <h3 class="display-4">Welcome!</h3>
            <p class="lead">This application displays maximum price of each grocery items, generates report and also allows searching by item name.</p>
        </div>
     );
    }
}

export default WelcomeComponent;
import React, {Component} from 'react';
import {Navbar, Nav} from 'react-bootstrap';
import {Link} from 'react-router-dom';

class NavigationBar extends Component {
    render() {
        return (
            <Navbar bg="dark" variant="dark">
                <Link to="" className="navbar-brand">Grocery App</Link> |{" "}
                <Nav className="me-auto">
                    <Link to="/maxPriceItem" className="navbar-link">Max Price of Items</Link> |{" |  "}
                    <Link to="/itemDetailsByName" className="navbar-link">Item Details</Link> |{" |  "}
                    <Link to="/itemReport" className="navbar-link">Item Report</Link> |{" |  "}
                </Nav>
            </Navbar>
        );
    }
}

export default NavigationBar;
